﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Irrelephant.MerchantSim.Common
{
    public static class Helper
    {
        public static Random GeneralRandomizer = new Random();

        public static T Sample<T>(this IEnumerable<T> sequence, Random randomizer = null)
        {
            return sequence.Skip((randomizer ?? GeneralRandomizer).Next(sequence.Count())).FirstOrDefault();
        }
    }
}
