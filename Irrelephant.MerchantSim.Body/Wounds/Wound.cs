﻿namespace Irrelephant.MerchantSim.Body.Wounds
{
    public class Wound
    {
        public int Severity;
        public int BleedSeverity;
        public int PainLevel;
        public bool Treatable = true;

        public void StackWith(Wound w)
        {
            Severity += w.Severity;
            BleedSeverity += w.BleedSeverity;
            PainLevel += w.BleedSeverity;
            if (!w.Treatable) Treatable = false;
        }
    }
}
