﻿using Irrelephant.MerchantSim.Body.BodyParts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Irrelephant.MerchantSim.Body
{
    public class BodyBase
    {
        public string Name { get; private set; }
        public IEnumerable<BodyPart> BodyParts { get; set; }
        public int Blood { get; private set; }
        public int MaxBlood { get; private set; }
        public bool NeedsBlood { get; set; }

        private int BloodRegenPerTick;

        public BodyBase(string name, IEnumerable<BodyPart> partsTree, int blood = 1, int bloodRegen = 1)
        {
            Name = name;
            BodyParts = partsTree;
            Blood = MaxBlood = blood;
            NeedsBlood = true;
            BloodRegenPerTick = bloodRegen;
        }

        private bool CheckVitalsRecursive(BodyPart p)
        {
            if (p.IsVital && p.State == Parameters.EnumHealthState.Destoyed) return false;
            return p.InternalParts.All(CheckVitalsRecursive) && p.SurfaceParts.All(CheckVitalsRecursive);
        }

        private bool NotEnoughBlood()
        {
            return NeedsBlood && (float)Blood / MaxBlood <= 0.5f;
        }

        private bool AnyVitalOrganDestroyed()
        {
            return !BodyParts.All(CheckVitalsRecursive);
        }

        public void Tick()
        {
            var totalBodyBleeds = BodyParts.Sum(GetAggregateBleedingRecursive);
            Blood -= totalBodyBleeds;
            Blood += BloodRegenPerTick;
            Blood = Math.Max(0, Math.Min(Blood, MaxBlood));
        }

        private int GetAggregateBleedingRecursive(BodyPart p)
        {
            var currentPartBleed = p.BodyPartWounds != null ? p.BodyPartWounds.BleedSeverity : 0;
            return currentPartBleed + p.InternalParts.Sum(GetAggregateBleedingRecursive) + p.SurfaceParts.Sum(GetAggregateBleedingRecursive);
        }

        public bool IsDead
        {
            get {
                return AnyVitalOrganDestroyed() || NotEnoughBlood();
            }
        }
    }
}
