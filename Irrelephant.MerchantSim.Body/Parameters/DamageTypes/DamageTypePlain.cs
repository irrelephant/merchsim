﻿using System;
using Irrelephant.MerchantSim.Body.BodyParts;
using Irrelephant.MerchantSim.Body.Wounds;

namespace Irrelephant.MerchantSim.Body.Parameters.DamageTypes
{
    class DamageTypePlain : DamageType
    {
        public float DamageFactor { get; private set; }
        public float BleedFactor { get; private set; }
        public float PainFactor { get; private set; }

        public DamageTypePlain(float dmg, float bleed, float pain)
        {
            DamageFactor = dmg;
            BleedFactor = bleed;
            PainFactor = pain;
        }

        public override void Apply(int points, BodyPart part)
        {
            part.Inflict(new Wound {
                Severity = (int)(points * DamageFactor),
                BleedSeverity = (int)(points * BleedFactor),
                PainLevel = (int)(points * PainFactor)
            });
        }
    }
}
