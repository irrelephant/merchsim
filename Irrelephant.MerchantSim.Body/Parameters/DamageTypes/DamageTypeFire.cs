﻿using System;
using Irrelephant.MerchantSim.Body.BodyParts;

namespace Irrelephant.MerchantSim.Body.Parameters.DamageTypes
{
    /// <summary>
    /// High overall damage. High damage to surface parts. Can sever limbs. Lower damage to internal parts.
    /// </summary>
    class DamageTypeFire : DamageType
    {
        public override void Apply(int points, BodyPart part)
        {
            throw new NotImplementedException();
        }
    }
}
