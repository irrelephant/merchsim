﻿using Irrelephant.MerchantSim.Body.BodyParts;
using System.Linq;
using Irrelephant.MerchantSim.Body.Wounds;
using Irrelephant.MerchantSim.Common;

namespace Irrelephant.MerchantSim.Body.Parameters.DamageTypes
{
    /// <summary>
    /// High overall damage. High damage to surface parts. Can sever limbs. Lower damage to internal parts.
    /// </summary>
    class DamageTypeSlashing : DamageType
    {
        private float targetPartDmgFactor = 0.7f;
        private float internalPartDmgFactor = 0.3f;

        public override void Apply(int points, BodyPart part)
        {
            ApplyRecursive((int)(points * targetPartDmgFactor), part, 0);
        }

        private float GetDmgFactorByLevel(int level)
        {
            if (level == 0) return targetPartDmgFactor;
            return internalPartDmgFactor;
        }

        private void ApplyRecursive(int points, BodyPart part, int level = 0)
        {
            if (part.SurfaceParts.Any())
            {
                part.SurfaceParts.Sample(DamageRandomizer).Inflict(new Wound
                {
                    Severity = points,
                    BleedSeverity = points,
                    PainLevel = points
                });
            }

            part.Inflict(new Wound
            {
                Severity = points,
                BleedSeverity = points,
                PainLevel = points
            });

            foreach (var internalPart in part.InternalParts)
            {
                ApplyRecursive((int)(points * internalPartDmgFactor / targetPartDmgFactor), internalPart, level + 1);
            }
        }
    }
}
