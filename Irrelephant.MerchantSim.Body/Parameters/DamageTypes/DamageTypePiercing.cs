﻿using Irrelephant.MerchantSim.Body.BodyParts;
using System.Linq;
using Irrelephant.MerchantSim.Body.Wounds;
using Irrelephant.MerchantSim.Common;

namespace Irrelephant.MerchantSim.Body.Parameters.DamageTypes
{
    /// <summary>
    /// High overall damage. High damage to surface parts. Can sever limbs. Lower damage to internal parts.
    /// </summary>
    class DamageTypePiercing : DamageType
    {
        public override void Apply(int points, BodyPart part)
        {
            if (part.SurfaceParts.Any())
            {
                part.SurfaceParts.Sample(DamageRandomizer)
                    .Inflict(new Wound {
                        Severity = points,
                        BleedSeverity = points,
                        PainLevel = points
                    });
            }

            if (part.InternalParts.Any())
            {
                part.InternalParts.Sample(DamageRandomizer)
                    .Inflict(new Wound {
                        Severity = points,
                        BleedSeverity = points,
                        PainLevel = points
                    });
            }

            part.Inflict(new Wound
            {
                Severity = points,
                BleedSeverity = points,
                PainLevel = points
            });
        }
    }
}
