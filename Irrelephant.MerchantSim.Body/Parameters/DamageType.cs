﻿using Irrelephant.MerchantSim.Body.BodyParts;
using Irrelephant.MerchantSim.Body.Parameters.DamageTypes;
using System;

namespace Irrelephant.MerchantSim.Body.Parameters
{
    public abstract class DamageType
    {
        public static readonly Random DamageRandomizer = new Random();

        /// <summary>
        /// High overall damage. High damage to surface parts. Can sever limbs. Lower damage to internal parts.
        /// </summary>
        public static readonly DamageType Slashing = new DamageTypeSlashing();

        /// <summary>
        /// Low overall damage. Damaging internals and surface parts equally.
        /// </summary>
        public static readonly DamageType Piercing = new DamageTypePiercing();

        /// <summary>
        /// Medium overall damage. Strong against bones. High damage to internals. Can cause untreatable (internal) wounds.
        /// </summary>
        public static readonly DamageType Blunt = new DamageTypeBlunt();

        /// <summary>
        /// Medium overall damage. High wound pain. Only damages surface parts.
        /// </summary>
        public static readonly DamageType Fire = new DamageTypeFire();

        /// <summary>
        /// High overall damage. Treated as fire damage to the surface parts. Treated as blunt damage to internals. Can sever limbs.
        /// </summary>
        public static readonly DamageType Explosion = new DamageTypeExplosion();

        /// <summary>
        /// Generic untreatable damage type only damages the target body part.
        /// </summary>
        /// <param name="damage">Severity factor</param>
        /// <param name="bleed">Bleed factor</param>
        /// <param name="pain">Pain factor</param>
        /// <returns></returns>
        public static DamageType Plain(float damage, float bleed, float pain)
        {
            return new DamageTypePlain(damage, bleed, pain);
        }

        public abstract void Apply(int points, BodyPart part);
        protected DamageType() { }
    }
}
