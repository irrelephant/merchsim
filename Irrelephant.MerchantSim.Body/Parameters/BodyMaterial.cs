﻿namespace Irrelephant.MerchantSim.Body.Parameters
{
    public class BodyMaterial
    {
        public static readonly BodyMaterial Bone = new BodyMaterial();
        public static readonly BodyMaterial Flesh = new BodyMaterial();
        public static readonly BodyMaterial Goo = new BodyMaterial();

        private BodyMaterial() { }
    }
}
