﻿using Irrelephant.MerchantSim.Body.Parameters;
using Irrelephant.MerchantSim.Body.Wounds;
using System;
using System.Collections.Generic;

namespace Irrelephant.MerchantSim.Body.BodyParts
{
    public class BodyPart
    {
        public string Name { get; private set; }
        public BodyMaterial Material { get; private set; }

        private int health;
        public int Health {
            get { return health; }
            private set
            {
                health = Math.Min(MaxHealth, Math.Max(0, value));
            }
        }
        public int MaxHealth { get; private set; }
        public bool IsVital { get; private set; }

        public Wound BodyPartWounds;

        public EnumHealthState State {
            get
            {
                if (Health == MaxHealth) return EnumHealthState.OK;
                if (Health == 0) return EnumHealthState.Destoyed;

                var ratio = (float)Health / MaxHealth;

                if (ratio > 0.67f) return EnumHealthState.MinorWounds;
                if (ratio > 0.33f) return EnumHealthState.MajorWounds;
                return EnumHealthState.CriticalWounds;
            }
        }

        private List<BodyPart> internalParts;
        public IEnumerable<BodyPart> InternalParts { get { return internalParts; } }

        private List<BodyPart> surfaceParts;
        public IEnumerable<BodyPart> SurfaceParts { get { return surfaceParts; } }

        public BodyPart(string name, BodyMaterial material, int maxHealth = 1, bool isVital = false)
        {
            Name = name ?? "{Unnamed}";
            if (material == null) throw new ArgumentNullException("Body part must have a material.");
            Material = material;
            internalParts = new List<BodyPart>();
            surfaceParts = new List<BodyPart>();
            Health = MaxHealth = maxHealth;
            IsVital = isVital;
        }

        public void Inflict(Wound w)
        {
            if (BodyPartWounds != null)
                BodyPartWounds.StackWith(w);
            else
                BodyPartWounds = w;

            Health -= w.Severity;
        }

        public BodyPart AddInternalParts(IEnumerable<BodyPart> parts)
        {
            internalParts.AddRange(parts);
            return this;
        }

        public BodyPart AddSurfaceParts(IEnumerable<BodyPart> parts)
        {
            surfaceParts.AddRange(parts);
            return this;
        }
    }
}
