﻿using Irrelephant.MerchantSim.Body;
using Irrelephant.MerchantSim.Body.BodyParts;
using Irrelephant.MerchantSim.Body.Parameters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Irrelephant.MerchantSim.Tests
{
    [TestClass]
    public class BodyTests
    {
        private BodyBase beholder;

        public object DamageTypes { get; private set; }

        [TestInitialize]
        public void Before()
        {
            beholder = new BodyBase("The Beholder", new List<BodyPart>
            {
                new BodyPart("Head", BodyMaterial.Bone, maxHealth: 10, isVital: false)
                    .AddInternalParts(new List<BodyPart> {
                        new BodyPart("Brain", BodyMaterial.Goo, maxHealth: 3, isVital: true)
                            .AddInternalParts(new List<BodyPart> {
                                new BodyPart("Flying Core", BodyMaterial.Flesh, maxHealth: 5, isVital: false)
                            })
                    })
                    .AddSurfaceParts(new List<BodyPart> {
                        new BodyPart("Eye", BodyMaterial.Goo, maxHealth: 3, isVital: false)
                    })
            }, blood: 5, bloodRegen: 0);
        }

        [TestMethod]
        [TestCategory("Body")]
        public void BodyHasName()
        {
            Assert.AreEqual("Name", new BodyBase("Name", null).Name);
        }

        [TestMethod]
        [TestCategory("Body")]
        public void BodyContainsTargetableBodyParts()
        {
            Assert.AreEqual(1, beholder.BodyParts.Count());
        }

        [TestMethod]
        [TestCategory("Body")]
        public void BodyHasBloodStat()
        {
            Assert.AreEqual(5, beholder.Blood);
        }

        [TestMethod]
        [TestCategory("Body")]
        public void BodyIsDeadWhenVitalOrganIsDestoyed()
        {
            var justDamage = DamageType.Plain(1f, 0f, 0f);
            justDamage.Apply(5, beholder.BodyParts.First());
            Assert.IsFalse(beholder.IsDead);
            justDamage.Apply(5, beholder.BodyParts.First().InternalParts.First());
            Assert.IsTrue(beholder.IsDead);
        }

        [TestMethod]
        [TestCategory("Body")]
        public void BodyIsDeadWhenLowBlood()
        {
            var justBleed = DamageType.Plain(0f, 1f, 0f);
            justBleed.Apply(1, beholder.BodyParts.First());
            beholder.Tick();
            Assert.IsFalse(beholder.IsDead);
            beholder.Tick();
            Assert.IsFalse(beholder.IsDead);
            beholder.Tick();
            Assert.IsTrue(beholder.IsDead);
        }
    }
}
