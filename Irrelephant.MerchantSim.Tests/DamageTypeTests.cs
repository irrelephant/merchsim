﻿using Irrelephant.MerchantSim.Body.BodyParts;
using Irrelephant.MerchantSim.Body.Parameters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Irrelephant.MerchantSim.Tests
{
    [TestClass]
    public class DamageTypeTests
    {
        private BodyPart head;
        private BodyPart eyes;
        private BodyPart brain;

        [TestInitialize]
        public void Before()
        {
            head = new BodyPart("Head", BodyMaterial.Bone, maxHealth: 10);
            eyes = new BodyPart("Eyes", BodyMaterial.Goo, maxHealth: 5, isVital: false);
            brain = new BodyPart("Brain", BodyMaterial.Goo, maxHealth: 5, isVital: true);

            head.AddSurfaceParts(new List<BodyPart> { eyes });
            head.AddInternalParts(new List<BodyPart> { brain });
        }

        [TestMethod]
        [TestCategory("Damage Types")]
        public void SlashingDamageTypeWorksAsExpected()
        {
            DamageType.Slashing.Apply(10, head);
            Assert.AreEqual(EnumHealthState.MajorWounds, head.State);
            Assert.AreEqual(EnumHealthState.Destoyed, eyes.State);
            Assert.AreEqual(EnumHealthState.MajorWounds, brain.State);
        }

        [TestMethod]
        [TestCategory("Damage Types")]
        public void PiercingDamageTypeWorksAsExpected()
        {
            DamageType.Piercing.Apply(5, head);
            Assert.AreEqual(EnumHealthState.MajorWounds, head.State);
            Assert.AreEqual(EnumHealthState.Destoyed, eyes.State);
            Assert.AreEqual(EnumHealthState.Destoyed, brain.State);
        }

        [TestMethod]
        [TestCategory("Damage Types")]
        public void BluntDamageWorksAsExpected()
        {
            DamageType.Blunt.Apply(10, head);

            Assert.AreEqual(EnumHealthState.MinorWounds, head.State);
            Assert.AreEqual(EnumHealthState.MajorWounds, eyes.State);
            Assert.AreEqual(EnumHealthState.Destoyed, brain.State);
        }
    }
}
