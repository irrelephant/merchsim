﻿using Irrelephant.MerchantSim.Body.Wounds;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Irrelephant.MerchantSim.Tests
{
    [TestClass]
    public class WoundTests
    {
        [TestMethod]
        [TestCategory("Wounds")]
        public void ShouldHaveSeverity()
        {
            var w = new Wound {
                Severity = 5
            };

            Assert.AreEqual(5, w.Severity);
        }

        [TestMethod]
        [TestCategory("Wounds")]
        public void ShouldHaveBleedSeverity()
        {
            var w = new Wound
            {
                BleedSeverity = 5
            };

            Assert.AreEqual(5, w.BleedSeverity);
        }

        [TestMethod]
        [TestCategory("Wounds")]
        public void ShouldHavePainLevel()
        {
            var w = new Wound
            {
                PainLevel = 5
            };

            Assert.AreEqual(5, w.PainLevel);
        }

        [TestMethod]
        [TestCategory("Wounds")]
        public void ShouldHaveTreatabilityFlag()
        {
            var w = new Wound
            {
                Treatable = false
            };

            Assert.IsFalse(w.Treatable);
        }

        [TestMethod]
        [TestCategory("Wounds")]
        public void ShouldStack()
        {
            var w1 = new Wound
            {
                PainLevel = 5,
                BleedSeverity = 5,
                Severity = 5
            };

            var w2 = new Wound
            {
                PainLevel = 5,
                BleedSeverity = 5,
                Severity = 5,
                Treatable = false
            };

            w1.StackWith(w2);

            Assert.AreEqual(10, w1.PainLevel);
            Assert.AreEqual(10, w1.BleedSeverity);
            Assert.AreEqual(10, w1.Severity);
            Assert.IsFalse(w1.Treatable);
        }
    }
}
