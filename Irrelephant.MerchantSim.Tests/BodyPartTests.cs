﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Irrelephant.MerchantSim.Body.BodyParts;
using Irrelephant.MerchantSim.Body.Parameters;
using System.Collections.Generic;
using System.Linq;

namespace Irrelephant.MerchantSim.Tests
{
    [TestClass]
    public class BodyPartTests
    {
        [TestMethod]
        [TestCategory("Body part")]
        public void ShouldHaveName()
        {
            var bp = new BodyPart("Heart", BodyMaterial.Flesh);
            Assert.AreEqual("Heart", bp.Name);

            var bp2 = new BodyPart(null, BodyMaterial.Flesh);
            Assert.AreEqual("{Unnamed}", bp2.Name);
        }

        [TestMethod]
        [TestCategory("Body part")]
        public void ShouldHaveVitalityFlag()
        {
            var bp = new BodyPart("Heart", BodyMaterial.Flesh, isVital: true);
            Assert.AreEqual(true, bp.IsVital);
        }

        [TestMethod]
        [TestCategory("Body part")]
        public void ShouldHaveMaterial()
        {
            var bp = new BodyPart("Arm", BodyMaterial.Bone);
            Assert.AreEqual(BodyMaterial.Bone, bp.Material);
        }

        [TestMethod]
        [TestCategory("Body part")]
        public void ShouldHaveHealth()
        {
            var bp = new BodyPart("Heart", BodyMaterial.Flesh);
            Assert.AreEqual(1, bp.Health);
            Assert.AreEqual(bp.Health, bp.MaxHealth);
        }

        [TestMethod]
        [TestCategory("Body part")]
        public void ShouldHaveOkStateByDefault()
        {
            var bp = new BodyPart("BP", BodyMaterial.Flesh);
            Assert.AreEqual(EnumHealthState.OK, bp.State);
        }

        [TestMethod]
        [TestCategory("Body part")]
        public void CanContainOtherBodyParts()
        {
            var bp = new BodyPart("Ribcage", BodyMaterial.Bone);
            bp.AddInternalParts(new List<BodyPart> {
                new BodyPart("Heart", BodyMaterial.Flesh),
                new BodyPart("Left Lung", BodyMaterial.Flesh),
                new BodyPart("Right Lung", BodyMaterial.Flesh)
            });

            Assert.AreEqual(3, bp.InternalParts.Count());
        }

        [TestMethod]
        [TestCategory("Body part")]
        public void CanHaveBodyPartsOnTheSurface()
        {
            var bp = new BodyPart("Head", BodyMaterial.Bone);
            bp.AddSurfaceParts(new List<BodyPart>
            {
                new BodyPart("Left Eye", BodyMaterial.Goo),
                new BodyPart("Right Eye", BodyMaterial.Goo)
            });

            Assert.AreEqual(2, bp.SurfaceParts.Count());
        }
    }
}
